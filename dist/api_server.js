"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ApiServer = void 0;

var _routes = require("./routes.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var restify = require('restify');

var plugins = restify.plugins;

var errors = require('restify-errors');

var corsMiddleware = require('restify-cors-middleware');

var restifyJwt = require('restify-jwt-community');

var jwt = require('jsonwebtoken');

var restifyValidation = require('node-restify-validation');

var pkg = require('./package.json');

var ApiServer =
/*#__PURE__*/
function () {
  function ApiServer(config, log) {
    var _this = this;

    _classCallCheck(this, ApiServer);

    this.config = Object.assign({
      log: log
    }, config.http);
    this.log = log;
    this.requestId = Date.now();
    this.server = restify.createServer(this.config);
    this.server.on('uncaughtException', this._uncaughtExceptionHandler.bind(this));
    this.server.pre(this._assignApiVersion.bind(this));
    this.server.pre(this._assignRequestId.bind(this));
    this.server.pre(this._logRequest.bind(this));

    if (this.config.CORS) {
      if (!this.config.CORS.hasOwnProperty('allowHeaders') && this.config.CORS.hasOwnProperty('headers')) {
        // upgrade path for config files
        this.config.CORS.allowHeaders = this.config.CORS.headers;
      }

      var cors = new corsMiddleware(this.config.CORS);
      this.server.pre(cors.preflight);
      this.server.use(cors.actual);
      this.server.on('MethodNotAllowed', this._methodNotAllowedHandler.bind(this));
    }

    this.server.use(plugins.acceptParser(this.server.acceptable));
    this.server.use(plugins.authorizationParser());
    this.server.use(plugins.dateParser());
    this.server.use(plugins.queryParser({
      mapParams: false
    }));
    this.server.use(plugins.bodyParser({
      mapParams: false
    }));
    this.server.use(plugins.gzipResponse());
    this.server.use(this._logBodyData.bind(this));

    if (this.config.jwt) {
      var jwtRsa = (this.config.jwt.pubKey || this.config.jwt.secret.startsWith('-----BEGIN CERTIFICATE-----')) && restifyJwt({
        secret: this.config.jwt.pubKey
      });
      var jwtHs = this.config.jwt.secret && restifyJwt({
        secret: this.config.jwt.secret
      });
      this.server.use(function (req, res, next) {
        if (_this.config['jwt-exclude-paths'] && _this.config['jwt-exclude-paths'].some(function (path) {
          return req.path().startsWith(path);
        })) {
          return next();
        }

        var auth = req.headers.authorization && req.headers.authorization.split(' ');

        if (!auth || auth.length === 0) {
          return next(new errors.UnauthorizedError('Missing Authorization header'));
        }

        if (auth[0].toLowerCase() !== 'bearer') {
          return next(new errors.UnauthorizedError('Unsupported authorization scheme'));
        }

        try {
          var token = jwt.decode(auth[1], {
            complete: true
          });

          if (token.header.kid && jwtRsa) {
            _this.log.info('RSA token');

            jwtRsa(req, res, next);
          } else if (jwtHs) {
            _this.log.info('HS token');

            jwtHs(req, res, next);
          } else {
            _this.log.info('Unsupported token algorithm');

            return next(new errors.UnauthorizedError('Unsupported token algorithm'));
          }
        } catch (err) {
          return next(new errors.UnauthorizedError('Unable to decode token'));
        }
      });

      if (this.config.hasOwnProperty('jwt-required-service')) {
        this.server.use(this._ensureJWTService.bind(this));
      }

      if (this.config.hasOwnProperty('jwt-required-services')) {
        this.server.use(this._ensureJWTServices.bind(this));
      }

      if (this.config.hasOwnProperty('jwt-accepted-claims') || this.config.hasOwnProperty('jwt-required-claims')) {
        this.server.use(this._ensureJWTClaims.bind(this));
      }
    }

    this.server.use(function (req, res, next) {
      if (req.user && req.route.hasOwnProperty('required_claim_bits')) {
        var keys = Object.keys(req.route.required_claim_bits);

        for (var i = 0; i < keys.length; i++) {
          var claim_name = keys[i];
          var req_claim = req.route.required_claim_bits[claim_name];

          if (!req.user.hasOwnProperty(claim_name)) {
            return next(new errors.UnauthorizedError('Failed required_claim_bits. Claim missing: ' + claim_name));
          }

          var user_claim = req.user[claim_name];

          if ((user_claim & req_claim) !== req_claim) {
            return next(new errors.UnauthorizedError('Failed required_claim_bits. Missing one or more bits of: ' + claim_name));
          }
        }
      }

      return next();
    });
    this.server.use(restifyValidation.validationPlugin({
      errorsAsArray: true,
      forbidUndefinedVariables: true,
      handleError: function handleError(res, errors) {
        res.send(409, {
          status: 'Validation failed',
          errors: errors
        });
        return res.end();
      }
    }));
  } // public methods


  _createClass(ApiServer, [{
    key: "addRoutes",
    value: function addRoutes(routes) {
      if (!(routes instanceof _routes.Routes)) {
        throw new Error('Unsupported type of routes class');
      }

      routes.registerRoutes(this.server);
    }
  }, {
    key: "start",
    value: function start() {
      var _this2 = this;

      return new Promise(function (resolve, reject) {
        _this2.server.on('error', function (err) {
          return reject(err);
        });

        _this2.server.listen(_this2.config.port, _this2.config.address || '0.0.0.0', function () {
          _this2.log.info('%s listening at %s', _this2.server.name, _this2.server.url);

          return resolve();
        });
      });
    } // private methods

  }, {
    key: "_assignApiVersion",
    value: function _assignApiVersion(req, res, next) {
      res.setHeader('X-AllBinary-Api-Version', pkg.version);
      return next();
    }
  }, {
    key: "_assignRequestId",
    value: function _assignRequestId(req, res, next) {
      req.request_id = 'req_' + this.requestId++;
      req.log = this.log.child({
        request_id: req.request_id
      });
      res.request_id = req.request_id;
      res.setHeader('X-Request-Id', req.request_id);
      res.log = req.log;
      return next();
    }
  }, {
    key: "_logRequest",
    value: function _logRequest(req, res, next) {
      req.log.info(req.method, req.url, req.headers);
      return next();
    }
  }, {
    key: "_logBodyData",
    value: function _logBodyData(req, res, next) {
      var method = req.method.toLowerCase();

      switch (method) {
        case 'post':
        case 'put':
        case 'patch':
          req.log.debug(req.body);
          break;
      }

      return next();
    }
  }, {
    key: "_ensureJWTService",
    value: function _ensureJWTService(req, res, next) {
      if (req.user) {
        var req_svc = this.config['jwt-required-service'];

        if (req_svc) {
          if (!req.user.svc) {
            return next(new errors.NotAuthorizedError('Presented token contained to service claim'));
          }

          if (req.user.svc !== req_svc) {
            return next(new errors.NotAuthorizedError('Presented token was issued for service not allowed to talk to this API'));
          }
        }
      }

      return next();
    }
  }, {
    key: "_ensureJWTServices",
    value: function _ensureJWTServices(req, res, next) {
      if (req.user) {
        var req_svc = this.config['jwt-required-services'];

        if (req_svc) {
          if (!req.user.svc) {
            return next(new errors.NotAuthorizedError('Presented token contained to service claim'));
          }

          if (Array.isArray(req_svc) && req_svc.length > 1) {
            if (req_svc.indexOf(req.user.svc) === -1) {
              return next(new errors.NotAuthorizedError('Presented token was issued for service not allowed to talk to this API'));
            }
          }
        }
      }

      return next();
    }
  }, {
    key: "_ensureJWTClaims",
    value: function _ensureJWTClaims(req, res, next) {
      var accepted_claims = this.config.hasOwnProperty('jwt-accepted-claims') ? this.config['jwt-accepted-claims'] : [];
      var required_claims = this.config.hasOwnProperty('jwt-required-claims') ? this.config['jwt-required-claims'] : [];

      if (req.user) {
        var claims = Object.keys(req.user);

        if (required_claims.length) {
          for (var i = 0; i < required_claims.length; i++) {
            var req_claim = required_claims[i];

            if (claims.indexOf(req_claim) === -1) {
              return next(new errors.NotAuthorizedError('Presented token lacks a required claim: ' + req_claim));
            }
          }
        } else if (accepted_claims.length) {
          for (var _i = 0; _i < accepted_claims.length; _i++) {
            var acc_claim = accepted_claims[_i];

            if (claims.indexOf(acc_claim) !== -1) {
              return next();
            }
          }

          return next(new errors.NotAuthorizedError('Presented token contains no accepted claims'));
        }
      }

      return next();
    }
  }, {
    key: "_uncaughtExceptionHandler",
    value: function _uncaughtExceptionHandler(req, res, route, err) {
      this.log.error(err);
    }
  }, {
    key: "_methodNotAllowedHandler",
    value: function _methodNotAllowedHandler(req, res) {
      // https://github.com/restify/node-restify/issues/284#issuecomment-11972985
      //
      if (req.method.toLowerCase() === 'options') {
        var allowHeaders = ['Accept', 'Accept-Version', 'Authorization', 'Content-Type', 'Api-Version', 'X-Requested-With'];

        if (res.methods.indexOf('OPTIONS') === -1) {
          res.methods.push('OPTIONS');
        }

        res.header('Access-Control-Allow-Credentials', true);
        res.header('Access-Control-Allow-Headers', allowHeaders.join(', '));
        res.header('Access-Control-Allow-Methods', res.methods.join(', '));
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        return res.send(204);
      }

      return res.send(new errors.MethodNotAllowedError());
    }
  }]);

  return ApiServer;
}();

exports.ApiServer = ApiServer;