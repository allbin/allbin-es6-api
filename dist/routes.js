"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Routes = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Routes =
/*#__PURE__*/
function () {
  function Routes() {
    _classCallCheck(this, Routes);
  }

  _createClass(Routes, [{
    key: "successResponse",
    value: function successResponse(res, next, options) {
      return function (data) {
        if (options && options.cache_max_age) {
          res.header('Cache-Control', "private, max-age: ".concat(options.cache_max_age));
        }

        res.json(200, data);
        return next();
      };
    }
  }, {
    key: "errorResponse",
    value: function errorResponse(res, next) {
      return function (err) {
        res.log.error(err, err.stack);
        return next(err);
      };
    }
  }, {
    key: "registerRoutes",
    value: function registerRoutes(app) {
      throw new Error('Not implemented');
    }
  }]);

  return Routes;
}();

exports.Routes = Routes;