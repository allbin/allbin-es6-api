'use strict';

import ApiServer from './index.js';
// import SomeRoutes from './some_routes.js'
// import SomeController from './some_controller.js'

let config = require('./example_config.js');

let bunyan = require('bunyan');
let log = bunyan.createLogger(config.log);

let app = new ApiServer(config, log);


// let mongodb = require('mongoskin');
// let db = mongodb.db(config.db.mongo_url, config.db.mongo_connect_opts);

// app.addRoutes(new SomeRoutes(new SomeController(db)));

app.start().then(() => {
    log.info("Ready for action");
}).catch((err) => {
    log.error("Sadness", err);
});

