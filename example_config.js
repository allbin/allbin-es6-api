module.exports = {
    "log": {
        "name": "example-api",
        "level": "trace",
        "streams": [
            {
                "level": "debug",
                "stream": process.env.LOG || process.stdout
            },
            {
                "level": "error",
                "path": process.env.ERROR_LOG || "./error.log"
            }
        ]
    },
    "http": {
        "CORS": {
            "credentials": false,
            "origins": ['*'],
            "headers": ['x-requested-with']
        },
        "jwt": process.env.JWT_CONFIG ? JSON.parse(process.env.JWT_CONFIG) : null,
        "name": "example-api",
        "port": process.env.PORT || 50000,
        "jwt-accepted-claims": process.env.JWT_ACCEPTED_CLAIMS ? JSON.parse(process.env.JWT_ACCEPTED_CLAIMS) : ["example-api"]
    },
};
