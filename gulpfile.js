const gulp = require('gulp');
const babel = require('gulp-babel');
const eslint = require('gulp-eslint');
const rimraf = require('rimraf');

function clean(cb) {
    rimraf('{dist,lib}', cb);
}

function build_package() {
    return gulp
        .src(['package.json', 'package-lock.json'])
        .pipe(gulp.dest('dist'));
}

function build_source(cb) {
    return gulp
        .src('src/**/*.js')
        .pipe(babel())
        .pipe(gulp.dest('dist'));
}

const build = gulp.series(clean, build_package, build_source);

exports.default = build;
exports.build = build;
exports.clean = clean;
