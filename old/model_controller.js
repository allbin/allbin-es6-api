let ObjectId = require('mongoskin').ObjectId;

let errors = require('restify').errors;

export class ModelController {

    constructor(db, modelName) {
        this.db = db;
        this.collectionName = modelName.endsWith('s') ? modelName + 'es' : modelName + 's';

        this.collection = this.db.collection(this.collectionName);
    }

    _map_id(unit) {
        unit.id = unit._id;
        delete unit._id;
        return unit;
    }

    list() {
        return new Promise((resolve, reject) => {
            this.collection.find({}, (err, units) => {
                if (err) {
                    return reject(err);
                }
                if (units === null) {
                    return reject(new errors.NotFoundError('Not found'));
                }
                units.toArray().then((us) => {
                    return resolve(us.map((x) => { return this._map_id(x); }));
                });
            });
        });
    }

    del(id) {
        return new Promise((resolve, reject) => {
            this.collection.findOne({_id: ObjectId(id)}, (err, unit) => {
                if (err) {
                    return reject(err);
                }
                if (unit === null) {
                    return reject(new errors.NotFoundError('Not found'));
                }

                this.collection.remove({_id: ObjectId(id)}, (err, result) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(this._map_id(unit));
                });
            });
        });
    }

    get(id) {
        return new Promise((resolve, reject) => {
            this.collection.findOne({_id: ObjectId(id)}, (err, unit) => {
                if (err) {
                    return reject(err);
                }
                if (unit === null) {
                    return reject(new errors.NotFoundError('Not found'));
                }
                return resolve(this._map_id(unit));
            });
        });
    }

    create(data) {
        return new Promise((resolve, reject) => {
            this.collection.insert(data, (err, result) => {
                if (err) {
                    return reject(err);
                }
                this.collection.findOne({_id: ObjectId(result.insertedIds[0])}, (err, unit) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(this._map_id(unit));
                });
            });
        });
    }

    update(id, data) {
        return new Promise((resolve, reject) => {
            this.collection.findOne({_id: ObjectId(id)}, (err, unit) => {
                if (err) {
                    return reject(err);
                }
                if (unit === null) {
                    return reject(new errors.NotFoundError('Not found'));
                }

                this.collection.update({_id: ObjectId(id)}, data, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    this.collection.findOne({_id: ObjectId(id)}, (err, updated_unit) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve(this._map_id(updated_unit));
                    });
                });
            });
        });
    }

    modify(id, data) {
        let query = {};

        // merge-update. don't replace, just update all keys
        Object.keys(data).forEach((key) => {
            query.$set[key] = data[key];
        });

        return this.update(id, query);
    }

    patch(id, data) {
        let query = {};

        // NOTE: proper array-handling currently only exists for a depth of 1,
        // meaning arrays that are immediate children of the 'data' object
        //
        Object.keys(data).forEach((key) => {
            if (Array.isArray(data[key])) {
                if (typeof query.$push === 'undefined') {
                    query.$push = {};
                }
                query.$push[key] = {
                    $each: data[key]
                };
            }
            else {
                if (typeof query.$set === 'undefined') {
                    query.$set = {};
                }
                query.$set[key] = data[key];
            }
        });

        return this.update(id, query);
    }
}
