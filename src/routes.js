export class Routes {
  successResponse(res, next, options) {
    return data => {
      if (options && options.cache_max_age) {
        res.header(
          'Cache-Control',
          `private, max-age: ${options.cache_max_age}`,
        );
      }
      res.json(200, data);
      return next();
    };
  }

  errorResponse(res, next) {
    return err => {
      res.log.error(err, err.stack);
      return next(err);
    };
  }

  registerRoutes(app) {
    throw new Error('Not implemented');
  }
}
